package main

import "net/http"

func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("Golang Redis orderbook prototype"))
	})

	http.ListenAndServe(":80", nil)
}
