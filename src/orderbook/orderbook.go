package orderbook

import (
	"github.com/go-redis/redis"
	"strconv"
	"strings"
)

const (
	OrderBookSortedSet = "z:orderbook"
)

type (
	OrderBookItem struct {
		Quantity float64
		Price    float64
	}

	DeleteOrderBookItems func(redisClient *redis.Client, items ...OrderBookItem)
)

func StoreOrderBookItems(redisClient *redis.Client, items ...OrderBookItem) {
	zMembers := make([]redis.Z, len(items))

	for i, item := range items {
		zMembers[i] = redis.Z{
			Member: OrderBookItemToMember(item),
			Score:  item.Price,
		}
	}

	redisClient.ZAdd(OrderBookSortedSet, zMembers...)
}

func StoreOrderBookItem(redisClient *redis.Client, item OrderBookItem) {
	redisClient.ZAdd(OrderBookSortedSet, redis.Z{
		Member: OrderBookItemToMember(item),
		Score:  item.Price,
	})
}

func FetchOrderBookItems(redisClient *redis.Client) ([]OrderBookItem, error) {
	members, err := redisClient.ZRange(OrderBookSortedSet, 0, -1).Result()

	if err != nil {
		return nil, err
	}

	result := make([]OrderBookItem, len(members))

	for i, member := range members {
		result[i] = OrderBookItemParse(member)
	}

	return result, nil
}

func DeleteOrderBookItemsByScan(redisClient *redis.Client, items ...OrderBookItem) {
	members := make([]interface{}, 0, len(items))

	for i := range items {
		pattern := OrderBookItemToMemberString(FormatFloat(items[i].Price), "*")

		// result like [member1, score1, member2, score2 ...]
		iterator := redisClient.ZScan(OrderBookSortedSet, 0, pattern, 8192).Iterator()

		// pass every odd
		need := true

		for iterator.Next() {
			if need {
				members = append(members, iterator.Val())
			}

			need = !need
		}
	}

	redisClient.ZRem(OrderBookSortedSet, members...)
}

func DeleteOrderBookItemsByScore(redisClient *redis.Client, items ...OrderBookItem) {
	members := make([]interface{}, 0, len(items))

	for i := range items {
		price := FormatFloat(items[i].Price)

		// result like [member1, score1, member2, score2 ...]
		result, err := redisClient.ZRangeByScore(OrderBookSortedSet, redis.ZRangeBy{
			Min: price,
			Max: price,
		}).Result()

		if err != nil {
			continue
		}

		size := len(result)

		for j := 0; j < size; j++ {
			members = append(members, result[j])
		}
	}

	redisClient.ZRem(OrderBookSortedSet, members...)
}

func DeleteOrderBookItemsByScoreDirect(redisClient *redis.Client, items ...OrderBookItem) {
	if len(items) == 1 {
		price := FormatFloat(items[0].Price)

		redisClient.ZRemRangeByScore(OrderBookSortedSet, price, price)
	} else {
		DeleteOrderBookItemsByScore(redisClient, items...)
	}
}

func OrderBookItemToMember(item OrderBookItem) string {
	return OrderBookItemToMemberString(FormatFloat(item.Price), FormatFloat(item.Quantity))
}

func OrderBookItemToMemberString(price, quantity string) string {
	return price + ":" + quantity
}

func OrderBookItemParse(source string) OrderBookItem {
	s := strings.Split(source, ":")

	price, quanity := s[0], s[1]

	return OrderBookItem{
		Price:    ParseFloat(price),
		Quantity: ParseFloat(quanity),
	}
}

func FormatFloat(f float64) string {
	return strconv.FormatFloat(f, 'f', -1, 64)
}

func ParseFloat(s string) float64 {
	result, _ := strconv.ParseFloat(s, 64)

	return result
}
