package orderbook

import (
	"github.com/go-redis/redis"
	"reflect"
	"testing"
)

var (
	testingRedisClient *redis.Client
)

func TestStoreOrderBookItem(t *testing.T) {
	redisClient := getTestingRedisClient()
	redisClient.Del(OrderBookSortedSet)

	StoreOrderBookItems(
		redisClient,
		OrderBookItem{
			Quantity: 5,
			Price:    1,
		},
		OrderBookItem{
			Quantity: 7,
			Price:    2,
		},
	)

	StoreOrderBookItem(
		redisClient,
		OrderBookItem{
			Quantity: 8,
			Price:    3,
		},
	)

	StoreOrderBookItems(
		redisClient,
		OrderBookItem{Quantity: 9, Price: 7},
		OrderBookItem{Quantity: 9, Price: 8},
		OrderBookItem{Quantity: 9, Price: 9},
	)

	assertSortedSet(t, redisClient, OrderBookSortedSet, []redis.Z{
		{
			Member: "1:5",
			Score:  1,
		},
		{
			Member: "2:7",
			Score:  2,
		},
		{
			Member: "3:8",
			Score:  3,
		},
		{
			Member: "7:9",
			Score:  7,
		},
		{
			Member: "8:9",
			Score:  8,
		},
		{
			Member: "9:9",
			Score:  9,
		},
	})
}

func TestFetchOrderBookItems(t *testing.T) {
	redisClient := getTestingRedisClient()
	redisClient.Del(OrderBookSortedSet)

	items := []OrderBookItem{
		{
			Quantity: 5,
			Price:    1,
		},
		{
			Quantity: 9,
			Price:    3,
		},
		{
			Quantity: 7,
			Price:    2,
		},
	}

	StoreOrderBookItems(
		redisClient,
		items...,
	)

	expect := []OrderBookItem{
		{
			Quantity: 5,
			Price:    1,
		},
		{
			Quantity: 7,
			Price:    2,
		},
		{
			Quantity: 9,
			Price:    3,
		},
	}

	actual, err := FetchOrderBookItems(redisClient)
	assertSuccess(t, err)

	assertSameReflect(t, expect, actual)
}

func TestDeleteOrderBookItemsByScan(t *testing.T) {
	testDeleteOrderBookItems(t, DeleteOrderBookItemsByScan)
}

func TestDeleteOrderBookItemsByScore(t *testing.T) {
	testDeleteOrderBookItems(t, DeleteOrderBookItemsByScore)
}

func testDeleteOrderBookItems(t *testing.T, handler DeleteOrderBookItems) {
	redisClient := getTestingRedisClient()
	redisClient.Del(OrderBookSortedSet)

	StoreOrderBookItems(
		redisClient,
		[]OrderBookItem{
			{
				Quantity: 15,
				Price:    3,
			},
			{
				Quantity: 8,
				Price:    10,
			},
			{
				Quantity: 7,
				Price:    20,
			},
			{
				Quantity: 1,
				Price:    45,
			},
		}...,
	)

	handler(redisClient,
		OrderBookItem{
			Price: 10,
		},
		OrderBookItem{
			Price: 45,
		},
	)

	assertSortedSet(t, redisClient, OrderBookSortedSet, []redis.Z{
		{
			Member: "3:15",
			Score:  3,
		},
		{
			Member: "20:7",
			Score:  20,
		},
	})
}

func TestDeleteOrderBookItemsByScanWithDoubles(t *testing.T) {
	testDeleteOrderBookItemsWithDoubles(t, DeleteOrderBookItemsByScan)
}

func TestDeleteOrderBookItemsByScoreWithDoubles(t *testing.T) {
	testDeleteOrderBookItemsWithDoubles(t, DeleteOrderBookItemsByScore)
}

func TestDeleteOrderBookItemsByScoreDirectWithDoubles(t *testing.T) {
	testDeleteOrderBookItemsWithDoubles(t, DeleteOrderBookItemsByScoreDirect)
}

func testDeleteOrderBookItemsWithDoubles(t *testing.T, handler DeleteOrderBookItems) {
	t.Helper()

	redisClient := getTestingRedisClient()
	redisClient.Del(OrderBookSortedSet)

	doubles := 8192
	uniques := 4
	size := doubles + uniques

	items := make([]OrderBookItem, 0, size)

	items = append(
		items,
		OrderBookItem{
			Price:    1,
			Quantity: 2,
		},
		OrderBookItem{
			Price:    2,
			Quantity: 3,
		},
	)

	doublePrice := float64(1000)

	for i := 0; i < doubles; i++ {
		items = append(items, OrderBookItem{
			Price:    doublePrice,
			Quantity: float64(1 + i),
		})
	}

	items = append(
		items,
		OrderBookItem{
			Price:    80001,
			Quantity: 80002,
		},
		OrderBookItem{
			Price:    80002,
			Quantity: 80003,
		},
	)

	StoreOrderBookItems(redisClient, items...)

	actualCount, err := redisClient.ZCard(OrderBookSortedSet).Result()
	assertSuccess(t, err)

	assertSame(t, int64(size), actualCount)

	handler(redisClient, OrderBookItem{
		Price: doublePrice,
	})

	actualCount, err = redisClient.ZCard(OrderBookSortedSet).Result()
	assertSuccess(t, err)

	assertSame(t, int64(uniques), actualCount)
}

func getTestingRedisClient() *redis.Client {
	if testingRedisClient == nil {
		testingRedisClient = redis.NewClient(&redis.Options{
			Addr: "redis_common:6379",
			DB:   0,
		})
	}

	return testingRedisClient
}

func assertSortedSet(t *testing.T, redisClient *redis.Client, zSet string, expectMembers []redis.Z) {
	t.Helper()

	zMembers, err := redisClient.ZRangeWithScores(zSet, 0, -1).Result()

	assertSuccess(t, err)

	if !reflect.DeepEqual(expectMembers, zMembers) {
		t.Error("Expect same sorted set members")
		t.Log("Expect :", expectMembers)
		t.Log("But got:", zMembers)
	}
}

func assertSuccess(t testing.TB, err error) {
	t.Helper()

	if err != nil {
		t.Error("Got:", err)
	}
}

func assertSame(t testing.TB, expect, actual interface{}) {
	t.Helper()

	if expect != actual {
		t.Error("Expect same", ":", expect, "but got:", actual)
	}
}

func assertSameReflect(t testing.TB, expect, actual interface{}) {
	t.Helper()

	if !reflect.DeepEqual(expect, actual) {
		t.Error("Expect:", expect)
		t.Error("Got:", actual)
	}
}
