package orderbook

import (
	"testing"
)

func BenchmarkStoreOrderBookItems(b *testing.B) {
	redisClient := getTestingRedisClient()
	redisClient.Del(OrderBookSortedSet)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		f := float64(i)

		StoreOrderBookItems(
			redisClient,
			OrderBookItem{
				Quantity: f,
				Price:    f,
			},
		)
	}
	b.StopTimer()

	expectedCount := int64(b.N)
	actualCount, err := redisClient.ZCard(OrderBookSortedSet).Result()
	assertSuccess(b, err)

	assertSame(b, expectedCount, actualCount)
}

func BenchmarkStoreOrderBookItem(b *testing.B) {
	redisClient := getTestingRedisClient()
	redisClient.Del(OrderBookSortedSet)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		f := float64(i)

		StoreOrderBookItem(
			redisClient,
			OrderBookItem{
				Quantity: f,
				Price:    f,
			},
		)
	}
	b.StopTimer()

	expectedCount := int64(b.N)
	actualCount, err := redisClient.ZCard(OrderBookSortedSet).Result()
	assertSuccess(b, err)

	assertSame(b, expectedCount, actualCount)
}

func BenchmarkDeleteOrderBookItemsByScan(b *testing.B) {
	benchmarkDeleteOrderBookItem(b, DeleteOrderBookItemsByScan)
}

func BenchmarkDeleteOrderBookItemsByScore(b *testing.B) {
	benchmarkDeleteOrderBookItem(b, DeleteOrderBookItemsByScore)
}

func BenchmarkDeleteOrderBookItemsByScoreDirect(b *testing.B) {
	benchmarkDeleteOrderBookItem(b, DeleteOrderBookItemsByScoreDirect)
}

func benchmarkDeleteOrderBookItem(b *testing.B, handler DeleteOrderBookItems) {
	redisClient := getTestingRedisClient()
	redisClient.Del(OrderBookSortedSet)

	items := make([]OrderBookItem, b.N)
	for i := 0; i < b.N; i++ {
		f := float64(i)

		items[i] = OrderBookItem{
			Quantity: f,
			Price:    f,
		}
	}

	StoreOrderBookItems(redisClient, items...)

	expectedCount := int64(b.N)
	actualCount, err := redisClient.ZCard(OrderBookSortedSet).Result()
	assertSuccess(b, err)

	assertSame(b, expectedCount, actualCount)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		f := float64(i)

		handler(
			redisClient,
			OrderBookItem{
				Price: f,
			},
		)
	}
	b.StopTimer()

	expectedCount = int64(0)
	actualCount, err = redisClient.ZCard(OrderBookSortedSet).Result()
	assertSuccess(b, err)

	assertSame(b, expectedCount, actualCount)
}
