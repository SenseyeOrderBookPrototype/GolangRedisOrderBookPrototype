#!/usr/bin/env bash

if [ "$1" == "up" ]; then
    sudo docker-compose up -d
elif [ "$1" == "down" ]; then
    sudo docker-compose down
elif [ "$1" == "ps" ]; then
    sudo docker ps
elif [ "$1" == "app" ]; then
    sudo docker exec -it gr_ob_app_local sh
elif [ "$1" == "redis" ]; then
    sudo docker exec -it gr_ob_redis_local sh
fi
