# GolangRedisOrderBookPrototype

```text
BenchmarkStoreOrderBookItems-4            100 000            249 483 ns/op             398 B/op         15 allocs/op
BenchmarkStoreOrderBookItem-4             100 000            176 380 ns/op             366 B/op         14 allocs/op
```

```text
BenchmarkDeleteOrderBookItemsByScan-4          	   10 000	   6 781 842 ns/op	     926 B/op	      29 allocs/op
BenchmarkDeleteOrderBookItemsByScore-4         	   10 000	     196 462 ns/op	     607 B/op	      22 allocs/op
BenchmarkDeleteOrderBookItemsByScoreDirect-4   	   30 000	      73 232 ns/op	     292 B/op	      10 allocs/op
```
